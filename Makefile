# Copyright (C) 2013-2023 Vincent Forest (vaplv@free.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

LIBNAME_STATIC = librsys.a
LIBNAME_SHARED = librsys.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

################################################################################
# RSys building
################################################################################
SRC =\
 src/clock_time.c\
 src/cstr.c\
 src/hash.c\
 src/image.c\
 src/library.c\
 src/logger.c\
 src/mem_allocator.c\
 src/mem_lifo_allocator.c\
 src/mem_proxy_allocator.c\
 src/pthread/pthread_condition.c\
 src/pthread/pthread_mutex.c\
 src/quaternion.c\
 src/str.c\
 src/text_reader.c
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

build_library: $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) \
	$$(if [ -n "$(LIBNAME)" ]; then\
	     echo "$(LIBNAME)";\
	   else\
	     echo "$(LIBNAME_SHARED)";\
	   fi)

$(DEP) $(OBJ): config.mk

$(LIBNAME_SHARED): $(OBJ)
	$(CC) $(CFLAGS_SO) -o $@ $(OBJ) $(LDFLAGS_SO) $(LIBS)

$(LIBNAME_STATIC): librsys.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

librsys.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CC) $(CFLAGS_SO) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) $(CFLAGS_SO) -DRSYS_SHARED_BUILD -c $< -o $@

################################################################################
# Installation
################################################################################
API =\
 src/algorithm.h\
 src/binary_heap.h\
 src/clock_time.h\
 src/condition.h\
 src/cstr.h\
 src/double2.h\
 src/double3.h\
 src/double4.h\
 src/double22.h\
 src/double33.h\
 src/double44.h\
 src/dynamic_array.h\
 src/dynamic_array_char.h\
 src/dynamic_array_double.h\
 src/dynamic_array_float.h\
 src/dynamic_array_int.h\
 src/dynamic_array_uchar.h\
 src/dynamic_array_u32.h\
 src/dynamic_array_u64.h\
 src/dynamic_array_uint.h\
 src/dynamic_array_size_t.h\
 src/dynamic_array_str.h\
 src/endianness.h\
 src/float2.h\
 src/float3.h\
 src/float4.h\
 src/float22.h\
 src/float33.h\
 src/float44.h\
 src/free_list.h\
 src/hash.h\
 src/hash_table.h\
 src/image.h\
 src/library.h\
 src/list.h\
 src/logger.h\
 src/math.h\
 src/mem_allocator.h\
 src/morton.h\
 src/mutex.h\
 src/quaternion.h\
 src/real2.h\
 src/real3.h\
 src/realX.h\
 src/realX_begin.h\
 src/realX_end.h\
 src/real22.h\
 src/real33.h\
 src/real44.h\
 src/realXY.h\
 src/realXY_begin.h\
 src/realXY_end.h\
 src/ref_count.h\
 src/rsys.h\
 src/signal.h\
 src/str.h\
 src/stretchy_array.h\
 src/text_reader.h

pkg:
	sed -e 's#@PREFIX@#$(PREFIX)#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    rsys.pc.in > rsys.pc

# Remove the include directive rather than setting it to "./src". to prevent
# the source directory from having a higher priority than the system include
# directories. In such a situation, the local "math.h" file could be included
# instead of the "math.h" header provided by the C standard library. Note that
# this is no longer a problem with the common pc file: the "math.h" file is
# installed in the "rsys" subdirectory, which is therefore a prefix of the
# header file allowing it to be distinguished from the header of the standard
# library
rsys-local.pc: rsys.pc.in
	sed -e '1,2d'\
	    -e 's#^libdir=.*#libdir=./#'\
	    -e 's#@PREFIX@#$(PREFIX)#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#-I$${includedir}##g'\
	    rsys.pc.in > $@

install: build_library pkg
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/" $(LIBNAME)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/rsys" COPYING README.md
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/pkgconfig" rsys.pc
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/rsys" $(API)

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/lib/$(LIBNAME)"
	rm -f "$(DESTDIR)$(PREFIX)/lib/pkgconfig/rsys.pc"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/rsys/COPYING"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/rsys/README.md"
	rm -f $$(echo $(API) | sed 's,src\/,$(DESTDIR)$(PREFIX)\/include\/rsys\/,g')

################################################################################
# Miscellaneous targets
################################################################################
all: build_library build_tests

clean: clean_test
	rm -f $(OBJ) $(TEST_OBJ) $(LIBNAME) librsys.o
	rm -f rsys.pc rsys-local.pc
	rm -f libtest_lib.so test_lib.o
	rm -f .test rsys.pc .test.ppm test_text_reader.txt test.ppm

distclean: clean
	rm -f $(DEP) $(TEST_DEP)

lint:
	shellcheck -o all make.sh

################################################################################
# Tests
################################################################################
TEST_SRC =\
 src/test_algorithm.c\
 src/test_atomic.c\
 src/test_binary_heap.c\
 src/test_condition.c\
 src/test_cstr.c\
 src/test_double22.c\
 src/test_double2.c\
 src/test_double33.c\
 src/test_double3.c\
 src/test_double44.c\
 src/test_double4.c\
 src/test_dynamic_array.c\
 src/test_endianness.c\
 src/test_float22.c\
 src/test_float2.c\
 src/test_float33.c\
 src/test_float3.c\
 src/test_float44.c\
 src/test_float4.c\
 src/test_free_list.c\
 src/test_func_name.c\
 src/test_hash_table.c\
 src/test_hash_sha256.c\
 src/test_image.c\
 src/test_library.c\
 src/test_list.c\
 src/test_logger.c\
 src/test_math.c\
 src/test_mem_allocator.c\
 src/test_misc.c\
 src/test_morton.c\
 src/test_mutex.c\
 src/test_quaternion.c\
 src/test_ref.c\
 src/test_signal.c\
 src/test_str.c\
 src/test_stretchy_array.c\
 src/test_text_reader.c\
 src/test_time.c\
 src/test_vmacros.c
TEST_OBJ = $(TEST_SRC:.c=.o)
TEST_DEP = $(TEST_SRC:.c=.d)

PKG_CONFIG_LOCAL = PKG_CONFIG_PATH="./:$${PKG_CONFIG_PATH}" $(PKG_CONFIG)
RSYS_CFLAGS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --cflags rsys-local.pc)
RSYS_LIBS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --libs rsys-local.pc)

build_tests: build_library $(TEST_DEP) .test
	@$(MAKE) -fMakefile -f.test $$(for i in $(TEST_DEP); do echo -f"$${i}"; done) test_bin

$(TEST_DEP) $(TEST_OBJ): config.mk

test: build_tests
	@$(SHELL) make.sh run_test $(TEST_SRC)

.test: Makefile
	@echo "Setup tests"
	@$(SHELL) make.sh config_test $(TEST_SRC) > .test

clean_test:
	@rm -f libtest_lib.so test_lib.o .test .test.ppm test_text_reader.txt test.ppm
	@$(SHELL) make.sh clean_test $(TEST_SRC)

src/test_algorithm.o \
src/test_atomic.o \
src/test_binary_heap.o \
src/test_double22.o \
src/test_double2.o \
src/test_double33.o \
src/test_double3.o \
src/test_double44.o \
src/test_double4.o \
src/test_dynamic_array.o \
src/test_endianness.o \
src/test_float22.o \
src/test_float2.o \
src/test_float33.o \
src/test_float3.o \
src/test_float44.o \
src/test_float4.o \
src/test_free_list.o \
src/test_func_name.o \
src/test_hash_sha256.o \
src/test_hash_table.o \
src/test_image.o \
src/test_library.o \
src/test_list.o \
src/test_logger.o \
src/test_math.o \
src/test_mem_allocator.o \
src/test_misc.o \
src/test_morton.o \
src/test_quaternion.o \
src/test_ref.o \
src/test_signal.o \
src/test_str.o \
src/test_stretchy_array.o \
src/test_text_reader.o \
src/test_time.o \
src/test_vmacros.o \
: config.mk rsys-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) $(RSYS_CFLAGS) -c $(@:.o=.c) -o $@

src/test_cstr.o: config.mk rsys-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) $(RSYS_CFLAGS) -Wno-long-long -c $(@:.o=.c) -o $@

src/test_condition.o src/test_mutex.o: config.mk rsys-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) $(RSYS_CFLAGS) -fopenmp -c $(@:.o=.c) -o $@

test_algorithm \
test_atomic \
test_endianness \
test_func_name \
test_misc \
test_morton \
test_ref \
test_vmacros \
: config.mk
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE)

test_double22 \
test_double2 \
test_double33 \
test_double3 \
test_double44 \
test_double4 \
test_float22 \
test_float2 \
test_float33 \
test_float3 \
test_float44 \
test_float4 \
test_math \
: config.mk
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) -lm

test_binary_heap\
test_cstr \
test_dynamic_array \
test_free_list \
test_hash_sha256 \
test_hash_table \
test_image \
test_library \
test_list \
test_logger \
test_mem_allocator \
test_signal \
test_str \
test_stretchy_array \
test_text_reader \
test_time \
: config.mk rsys-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS) $(RSYS_LIBS)

test_quaternion: config.mk rsys-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS) $(RSYS_LIBS) -lm

test_condition test_mutex: config.mk rsys-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS) $(RSYS_LIBS) -lm -fopenmp

test_lib.o: src/test_library.c src/rsys.h config.mk
	$(CC) $(CFLAGS_SO) -c src/test_library.c -DTEST_LIBRARY_BUILD_LIB -o $@

libtest_lib.so: test_lib.o config.mk
	$(CC) $(CFLAGS_SO) -o $@ test_lib.o $(LDFLAGS_SO)

test_library: libtest_lib.so
