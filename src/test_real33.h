/* Copyright (C) 2013-2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSys library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSys library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSys library. If not, see <http://www.gnu.org/licenses/>. */

#include "rsys.h"
#include <float.h>
#include <math.h>

#define REALX_DIMENSION__ 3
#define REALY_DIMENSION__ 3
#include "realXY_begin.h"

#define REAL REAL_TYPE__
#define REAL_COMPATIBLE REAL_TYPE_COMPATIBLE__

#define CHECK_REAL3(A, B)                                                      \
  {                                                                            \
    const REAL* a__ = (A);                                                     \
    const REAL* b__ = (B);                                                     \
    int i__;                                                                   \
    FOR_EACH(i__, 0, 3)                                                        \
      CHK(a__[i__] == b__[i__]);                                               \
  } (void)0
#define CHECK_REAL33(A, B)                                                     \
  {                                                                            \
    const REAL* a__ = (A);                                                     \
    const REAL* b__ = (B);                                                     \
    int i__;                                                                   \
    FOR_EACH(i__, 0, 9)                                                        \
      CHK(a__[i__] == b__[i__]);                                               \
  } (void)0

int
main(int argc, char** argv)
{
  REAL a[9], b[9], dst[9], c[9];
  REAL_COMPATIBLE d[9];
  int i;
  (void)argc, (void)argv;

  REALXY_FUNC__(set)(a, REALXY_FUNC__(splat)(c, -1.0));
  FOR_EACH(i, 0, 9) {
    CHK(a[i] == -1.0);
    CHK(c[i] == -1.0);
  }
  REALXY_CTOR__(a, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0);
  FOR_EACH(i, 0, 9) {
    CHK(a[i] == (REAL)i);
  }
  CHECK_REAL33
    (REALXY_FUNC__(set_identity)(a),
     REALXY_CTOR__(c, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0));
  CHECK_REAL33(a, REALXY_CTOR__(c, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0));
  CHK(REALXY_FUNC__(is_identity)(a) == 1);

  REALXY_FUNC__(set)(c, a);
  FOR_EACH(i, 0, REALX_DIMENSION__*REALY_DIMENSION__) {
    REAL_TYPE__ r = c[i];
    c[i] = c[i] + REAL_EPSILON__;
    CHK(REALXY_FUNC__(is_identity)(c) == 0);
    c[i] = r;
  }

  d[0] = (REAL_COMPATIBLE)0.1;
  d[1] = (REAL_COMPATIBLE)(1.0/3.0);
  d[2] = (REAL_COMPATIBLE)0.3;
  d[3] = (REAL_COMPATIBLE)-0.7;
  d[4] = (REAL_COMPATIBLE)0.9;
  d[5] = (REAL_COMPATIBLE)-0.41;
  d[6] = (REAL_COMPATIBLE)0.22;
  d[7] = (REAL_COMPATIBLE)-0.01;
  d[8] = (REAL_COMPATIBLE)0.02;

  REALXY_CTOR__(c,
    (REAL)(REAL_COMPATIBLE)0.1,
    (REAL)(REAL_COMPATIBLE)(1.0/3.0),
    (REAL)(REAL_COMPATIBLE)0.3,
    (REAL)(REAL_COMPATIBLE)-0.7,
    (REAL)(REAL_COMPATIBLE)0.9,
    (REAL)(REAL_COMPATIBLE)-0.41,
    (REAL)(REAL_COMPATIBLE)0.22,
    (REAL)(REAL_COMPATIBLE)-0.01,
    (REAL)(REAL_COMPATIBLE)0.02);
  CHECK_REAL33(REALXY_CAST__(dst, d), c);

  REALXY_FUNC__(splat)(a, -1.0);
  CHECK_REAL33
    (REALXY_FUNC__(set_row)(a, REALX_CTOR__(c, 0.0, 1.0, 2.0), 0),
     REALXY_CTOR__(c, 0.0, -1.0, -1.0, 1.0, -1.0, -1.0, 2.0, -1.0, -1.0));
  CHECK_REAL33(a, REALXY_CTOR__(c, 0.0, -1.0, -1.0, 1.0, -1.0, -1.0, 2.0, -1.0, -1.0));
  CHECK_REAL33
    (REALXY_FUNC__(set_row)(a, REALX_CTOR__(c, 3.0, 4.0, 5.0), 1),
     REALXY_CTOR__(c, 0.0, 3.0, -1.0, 1.0, 4.0, -1.0, 2.0, 5.0, -1.0));
  CHECK_REAL33(a, REALXY_CTOR__(c, 0.0, 3.0, -1.0, 1.0, 4.0, -1.0, 2.0, 5.0, -1.0));
  CHECK_REAL33
    (REALXY_FUNC__(set_row)(a, REALX_CTOR__(c, 6.0, 7.0, 8.0), 2),
     REALXY_CTOR__(c, 0.0, 3.0, 6.0, 1.0, 4.0, 7.0, 2.0, 5.0, 8.0));
  CHECK_REAL33(a, REALXY_CTOR__(c, 0.0, 3.0, 6.0, 1.0, 4.0, 7.0, 2.0, 5.0, 8.0));

  CHECK_REAL33
    (REALXY_FUNC__(transpose)(a, a),
     REALXY_CTOR__(c, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0));
  CHECK_REAL33(a, REALXY_CTOR__(c, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0));
  CHECK_REAL33
    (REALXY_FUNC__(transpose)(b, a),
     REALXY_CTOR__(c, 0.0, 3.0, 6.0, 1.0, 4.0, 7.0, 2.0, 5.0, 8.0));
  CHECK_REAL33(b, REALXY_CTOR__(c, 0.0, 3.0, 6.0, 1.0, 4.0, 7.0, 2.0, 5.0, 8.0));

  REALXY_FUNC__(splat)(a, -1.0);
  CHECK_REAL33
    (REALXY_FUNC__(set_col)(a, REALX_CTOR__(c, 0.0, 1.0, 2.0), 0),
     REALXY_CTOR__(c, 0.0, 1.0, 2.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0));
  CHECK_REAL33(a, REALXY_CTOR__(c, 0.0, 1.0, 2.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0));
  CHECK_REAL33
    (REALXY_FUNC__(set_col)(a, REALX_CTOR__(c, 3.0, 4.0, 5.0), 1),
     REALXY_CTOR__(c, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, -1.0, -1.0, -1.0));
  CHECK_REAL33(a, REALXY_CTOR__(c, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, -1.0, -1.0, -1.0));
  CHECK_REAL33
    (REALXY_FUNC__(set_col)(a, REALX_CTOR__(c, 6.0, 7.0, 8.0), 2),
     REALXY_CTOR__(c, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0));
  CHECK_REAL33(a, REALXY_CTOR__(c, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0));

  CHECK_REAL3(REALXY_FUNC__(row)(b, a, 0), REALX_CTOR__(c, 0.0, 3.0, 6.0));
  CHECK_REAL3(b, REALX_CTOR__(c, 0.0, 3.0, 6.0));
  CHECK_REAL3(REALXY_FUNC__(row)(b, a, 1), REALX_CTOR__(c, 1.0, 4.0, 7.0));
  CHECK_REAL3(b, REALX_CTOR__(c, 1.0, 4.0, 7.0));
  CHECK_REAL3(REALXY_FUNC__(row)(b, a, 2), REALX_CTOR__(c, 2.0, 5.0, 8.0));
  CHECK_REAL3(b, REALX_CTOR__(c, 2.0, 5.0, 8.0));

  CHECK_REAL3(REALXY_FUNC__(col)(b + 2, a, 0), REALX_CTOR__(c, 0.0, 1.0, 2.0));
  CHECK_REAL3(b + 2, REALX_CTOR__(c, 0.0, 1.0, 2.0));
  CHECK_REAL3(REALXY_FUNC__(col)(b + 1, a, 1), REALX_CTOR__(c, 3.0, 4.0, 5.0));
  CHECK_REAL3(b + 1, REALX_CTOR__(c, 3.0, 4.0, 5.0));
  CHECK_REAL3(REALXY_FUNC__(col)(b + 5, a, 2), REALX_CTOR__(c, 6.0, 7.0, 8.0));
  CHECK_REAL3(b + 5, REALX_CTOR__(c, 6.0, 7.0, 8.0));

  CHECK_REAL3(REALXY_FUNC__(col_ptr)(a, 0), REALX_CTOR__(c, 0.0, 1.0, 2.0));
  CHECK_REAL3(REALXY_FUNC__(col_ptr)(a, 1), REALX_CTOR__(c, 3.0, 4.0, 5.0));
  CHECK_REAL3(REALXY_FUNC__(col_ptr)(a, 2), REALX_CTOR__(c, 6.0, 7.0, 8.0));
  CHECK_REAL3(REALXY_FUNC__(col_cptr)(a, 0), REALX_CTOR__(c, 0.0, 1.0, 2.0));
  CHECK_REAL3(REALXY_FUNC__(col_cptr)(a, 1), REALX_CTOR__(c, 3.0, 4.0, 5.0));
  CHECK_REAL3(REALXY_FUNC__(col_cptr)(a, 2), REALX_CTOR__(c, 6.0, 7.0, 8.0));

  REALXY_CTOR__(a, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0);
  CHECK_REAL3(REALXY_REALX_FUNC__(mul)(dst, a, REALX_CTOR__(c, 1.0, 2.0, 3.0)),
    REALX_CTOR__(c, 30.0, 36.0, 42.0));
  CHECK_REAL3(dst, REALX_CTOR__(c, 30.0, 36.0, 42.0));
  CHECK_REAL3(REALX_REALXY_FUNC__(mul)(dst, REALX_CTOR__(c, 1.0, 2.0, 3.0), a),
    REALX_CTOR__(c, 14.0, 32.0, 50.0));
  CHECK_REAL3(dst, REALX_CTOR__(c, 14.0, 32.0, 50.0));
  CHECK_REAL3
    (REALXY_FUNC__(mul)(dst, a, -1.0),
     REALXY_CTOR__(c, -1.0, -2.0, -3.0, -4.0, -5.0, -6.0, -7.0, -8.0, -9.0));
  CHECK_REAL33(dst,
    REALXY_CTOR__(c, -1.0, -2.0, -3.0, -4.0, -5.0, -6.0, -7.0, -8.0, -9.0));

  REALXY_CTOR__(b, 2.0, 9.0, 8.0, 1.0, -2.0, 2.0, 1.0, -8.0, -4.0);
  CHECK_REAL33
    (REALXY_REALXY_FUNC__(mul)(dst, a, b),
     REALXY_CTOR__(c, 94.0, 113.0, 132.0, 7.0, 8.0, 9.0, -59.0, -70.0,-81.0));
	CHECK_REAL33(dst,
    REALXY_CTOR__(c, 94.0, 113.0, 132.0, 7.0, 8.0, 9.0, -59.0, -70.0,-81.0));
  CHECK_REAL33
    (REALXY_REALXY_FUNC__(mul)(a, a, b),
     REALXY_CTOR__(c, 94.0, 113.0, 132.0, 7.0, 8.0, 9.0, -59.0, -70.0, -81.0));
	CHECK_REAL33(a,
    REALXY_CTOR__(c, 94.0, 113.0, 132.0, 7.0, 8.0, 9.0, -59.0, -70.0, -81.0));
  REALXY_CTOR__(a, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0);
  CHECK_REAL33
    (REALXY_REALXY_FUNC__(mul)(b, a, b),
     REALXY_CTOR__(c, 94.0, 113.0, 132.0, 7.0, 8.0, 9.0, -59.0, -70.0, -81.0));
	CHECK_REAL33(b,
    REALXY_CTOR__(c, 94.0, 113.0, 132.0, 7.0, 8.0, 9.0, -59.0, -70.0, -81.0));
  REALXY_CTOR__(b, 2.0, 9.0, 8.0, 1.0, -2.0, 2.0, 1.0, -8.0, -4.0);

  REALXY_CTOR__(a, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0);
  REALXY_CTOR__(b, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0);
  CHECK_REAL33
    (REALXY_FUNC__(add)(dst, a, b),
     REALXY_CTOR__(c, 1.0, 3.0, 5.0, 7.0, 9.0, 11.0, 13.0, 15.0, 17.0));
  CHECK_REAL33(dst,
    REALXY_CTOR__(c, 1.0, 3.0, 5.0, 7.0, 9.0, 11.0, 13.0, 15.0, 17.0));
  CHECK_REAL33
    (REALXY_FUNC__(sub)(dst, dst, b),
     REALXY_CTOR__(c, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0));
	CHECK_REAL33(dst,
    REALXY_CTOR__(c, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0));
  CHECK_REAL33
    (REALXY_FUNC__(minus)(a, b),
     REALXY_CTOR__(c,  -1.0, -2.0, -3.0, -4.0, -5.0, -6.0, -7.0, -8.0, -9.0));
  CHECK_REAL33(a,
    REALXY_CTOR__(c,  -1.0, -2.0, -3.0, -4.0, -5.0, -6.0, -7.0, -8.0, -9.0));

  REALXY_FUNC__(set)(a, b);
  CHK(REALXY_FUNC__(eq)(a, b) == 1);
  REALXY_FUNC__(add)(a, a, REALXY_FUNC__(splat)(c, FLT_EPSILON));
  CHK(REALXY_FUNC__(eq)(a, b) == 0);
  CHK(REALXY_FUNC__(eq_eps)(a, b, FLT_EPSILON) == 1);
  CHK(REALXY_FUNC__(eq_eps)(a, b, FLT_EPSILON * (REAL)0.9) == 0);

  REALXY_CTOR__(a, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 3.0, -4.0, 9.0);
  CHK(REALXY_FUNC__(det)(a) == -60.0);
  CHK(REALXY_FUNC__(inverse)(b, a) == -60.0);
  REALXY_REALXY_FUNC__(mul)(dst, a, b);
  CHK(REALXY_FUNC__(eq_eps)(dst,
    REALXY_CTOR__(c, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0), 1.e-6f));
  CHK(REALXY_FUNC__(invtrans)(a, a) == -60.0);
  CHECK_REAL33
    (a, REALXY_CTOR__(c, b[0], b[3], b[6], b[1], b[4], b[7], b[2], b[5], b[8]));

  REALX_CTOR__(c, (REAL)0.66666667, (REAL)0.33333333, (REAL)0.66666667);
  REALXY_FUNC__(rotation_axis_angle)(a, c, (REAL)0.349066);
  CHK(REALXY_FUNC__(eq_eps)(a, REALXY_CTOR__(c,
    (REAL)0.966496, (REAL)0.241415, (REAL)-0.0872034,
    (REAL)-0.214612, (REAL)0.946393, (REAL)0.241415,
    (REAL)0.14081, (REAL)-0.214612, (REAL)0.966496), (REAL)1.e-6));

  return 0;
}

