/* Copyright (C) 2013-2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSys library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSys library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSys library. If not, see <http://www.gnu.org/licenses/>. */

#ifndef IO_C99_H
#define IO_C99_H

#include "rsys.h"

#ifdef COMPILER_CL

#include <stdarg.h>

#define snprintf snprintf_c99__
#define vsnprintf vsnprint_c99__

static INLINE int
vsnprint_c99__(char* str, size_t sz, const char* fmt, va_list arg_lst)
{
  int count = -1;

  if(sz)
    count = _vsnprintf_s(str, sz, _TRUNCATE, fmt, arg_lst);
  if(count == -1)
    count = _vscprintf(fmt, arg_lst);

  return count;
}

static INLINE int
snprintf_c99__(char* str, size_t sz, const char* fmt, ...)
{
  int count;
  va_list arg_lst;

  va_start(arg_lst, fmt);
  count = vsnprint_c99__(str, sz, fmt, arg_lst);
  va_end(arg_lst);

  return count;
}

#endif /* COMPILER_CL */

#endif /* IO_C99_H */
