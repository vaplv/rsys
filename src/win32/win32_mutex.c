/* Copyright (C) 2013-2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSys library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSys library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSys library. If not, see <http://www.gnu.org/licenses/>. */

#include "../mem_allocator.h"
#include "../mutex.h"
#include <Windows.h>

/*******************************************************************************
 * Mutex
 ******************************************************************************/
struct mutex*
mutex_create(void)
{
  LPCRITICAL_SECTION mutex = mem_alloc(sizeof(CRITICAL_SECTION));
  if(mutex)
    InitializeCriticalSection(mutex);
  return (struct mutex*)mutex;
}

void
mutex_destroy(struct mutex* mutex)
{
  ASSERT(mutex);
  DeleteCriticalSection((LPCRITICAL_SECTION)mutex);
  mem_rm(mutex);
}

void
mutex_lock(struct mutex* mutex)
{
  ASSERT(mutex);
  EnterCriticalSection((LPCRITICAL_SECTION)mutex);
}

void
mutex_unlock(struct mutex* mutex)
{
  ASSERT(mutex);
  LeaveCriticalSection((LPCRITICAL_SECTION)mutex);
}

/*******************************************************************************
 * Read Write mutex
 ******************************************************************************/
struct mutex_rw*
mutex_rw_create(void)
{
  FATAL("Missing Read/Write mutex implementation with Win32 thread API.\n");
  return NULL;
}

void
mutex_rw_destroy(struct mutex_rw* mutex)
{
  (void)mutex;
  FATAL("Missing Read/Write mutex implementation with Win32 thread API.\n");
}

void
mutex_rw_rlock(struct mutex_rw* mutex)
{
  (void)mutex;
  FATAL("Missing Read/Write mutex implementation with Win32 thread API.\n");
}

void
mutex_rw_wlock(struct mutex_rw* mutex)
{
  (void)mutex;
  FATAL("Missing Read/Write mutex implementation with Win32 thread API.\n");
}

void
mutex_rw_unlock(struct mutex_rw* mutex)
{
  (void)mutex;
  FATAL("Missing Read/Write mutex implementation with Win32 thread API.\n");
}

