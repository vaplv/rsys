/* Copyright (C) 2013-2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSys library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSys library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSys library. If not, see <http://www.gnu.org/licenses/>. */

#include "../condition.h"
#include "../mem_allocator.h"
#include <Windows.h>

struct cond*
cond_create(void)
{
  PCONDITION_VARIABLE cond = mem_alloc(sizeof(CONDITION_VARIABLE));
  if(cond)
    InitializeConditionVariable(cond);
  return (struct cond*)cond;
}

void
cond_destroy(struct cond* cond)
{
  ASSERT(cond);
  mem_rm(cond);
}

void
cond_wait(struct cond* cond, struct mutex* mutex)
{
  BOOL b;
  (void)b;
  ASSERT(cond);
  b = SleepConditionVariableCS
    ((PCONDITION_VARIABLE)cond, (PCRITICAL_SECTION)mutex, INFINITE);
  ASSERT(b != 0);
}

void
cond_signal(struct cond* cond)
{
  ASSERT(cond);
  WakeConditionVariable((PCONDITION_VARIABLE)cond);
}

void
cond_broadcast(struct cond* cond)
{
  ASSERT(cond);
  WakeAllConditionVariable((PCONDITION_VARIABLE)cond);
}

