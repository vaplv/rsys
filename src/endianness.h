/* Copyright (C) 2013-2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSys library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSys library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSys library. If not, see <http://www.gnu.org/licenses/>. */

#ifndef ENDIANNESS_H
#define ENDIANNESS_H

#include "rsys.h"

#if defined(COMPILER_GCC)
  #define BYTE_ORDER __BYTE_ORDER__
  #define LITTLE_ENDIAN __ORDER_LITTLE_ENDIAN__
  #define BIG_ENDIAN __ORDER_BIG_ENDIAN__
  #include <byteswap.h>
#elif defined(COMPILER_CL)
  #include <Windows.h>
  #define BYTE_ORDER REG_DWORD
  #define LITTLE_ENDIAN REG_DWORD_LITTLE_ENDIAN
  #define BIG_ENDIAN REG_DWORD_BIG_ENDIAN
#else
  #error "Undefined byte ordering macros"
#endif

#ifdef COMPILER_GCC
static FINLINE uint16_t byte_swap_16(const uint16_t ui) { return bswap_16(ui); }
static FINLINE uint32_t byte_swap_32(const uint32_t ui) { return bswap_32(ui); }
static FINLINE uint64_t byte_swap_64(const uint64_t ui) { return bswap_64(ui); }

#elif defined COMPILER_CL
static FINLINE uint16_t
byte_swap_16(const uint16_t ui)
{
  STATIC_ASSERT(sizeof(unsigned short) == sizeof(uint16_t),
    Unexpected_sizeof_ushort);
  return _byteswap_ushort(ui);
}

static FINLINE uint32_t
byte_swap_32(const uint32_t ui)
{
  STATIC_ASSERT(sizeof(unsigned long) == sizeof(uint32_t),
    Unexpected_sizeof_ushort);
  return _byteswap_ulong(ui);
}

static FINLINE uint64_t
byte_swap_64(const uint64_t ui)
{
  return _byteswap_uint64(ui);
}
#endif

static FINLINE uint16_t
little_endian_16(const uint16_t ui)
{
#if BYTE_ORDER == LITTLE_ENDIAN
  return ui;
#elif BYTE_ORDER == BIG_ENDIAN
  return byte_swap_16(ui);
#endif
}

static FINLINE uint32_t
little_endian_32(const uint32_t ui)
{
#if BYTE_ORDER == LITTLE_ENDIAN
  return ui;
#elif BYTE_ORDER == BIG_ENDIAN
  return byte_swap_32(ui);
#endif
}

static FINLINE uint64_t
little_endian_64(const uint64_t ui)
{
#if BYTE_ORDER == LITTLE_ENDIAN
  return ui;
#elif BYTE_ORDER == BIG_ENDIAN
  return byte_swap_64(ui);
#endif
}

static FINLINE uint16_t
big_endian_16(const uint16_t ui)
{
#if BYTE_ORDER == LITTLE_ENDIAN
  return byte_swap_16(ui);
#elif BYTE_ORDER == BIG_ENDIAN
  return ui;
#endif
}

static FINLINE uint32_t
big_endian_32(const uint32_t ui)
{
#if BYTE_ORDER == LITTLE_ENDIAN
  return byte_swap_32(ui);
#elif BYTE_ORDER == BIG_ENDIAN
  return ui;
#endif
}

static FINLINE uint64_t
big_endian_64(const uint64_t ui)
{
#if BYTE_ORDER == LITTLE_ENDIAN
  return byte_swap_64(ui);
#elif BYTE_ORDER == BIG_ENDIAN
  return ui;
#endif
}

#endif /* ENDIANNESS_H */
