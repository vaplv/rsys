/* Copyright (C) 2013-2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSys library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSys library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSys library. If not, see <http://www.gnu.org/licenses/>. */

#ifndef MATH_H
#define MATH_H

#include "rsys.h"
#include <float.h>
#include <math.h>

#define MMAX(A, B) ((A) > (B) ? (A) : (B))
#define MMIN(A, B) ((A) < (B) ? (A) : (B))
#define CLAMP(A, Min, Max) MMIN(MMAX(Min, A), Max)
#define IS_POW2(A) (((A) & ((A)-1)) == 0 && (A) > 0)
#define INF (DBL_MAX + DBL_MAX)
#define NaN (-(INF*0))
#define IS_INF(X) ((X==INF) || (X==-INF))
#define IS_NaN(X) (!((X)==(X)))
#define PI 3.14159265358979323846
#define RCP_PI 0.31830988618379067154	/* 1/pi */
#define SQRT2 1.41421356237309504880 /* sqrt(2) */
#define MDEG2RAD(Deg) ((Deg)*PI/180.0)
#define MRAD2DEG(Rad) ((Rad)*180.0/PI)

static FINLINE size_t
round_up_pow2(const size_t i)
{
  if(IS_POW2(i)) {
    return i;
  } else if(!i) {
    return 1;
  } else {
    size_t j = i - 1;
    unsigned k;
    for(k = 1; k < sizeof(int)*8; k <<= 1)
      j |= j >> k;
    return j + 1;
  }
}

static FINLINE int
log2i(const int i)
{
  union { float f; int32_t i; } ucast;
  ASSERT(i != 0);
  ucast.f = (float)i;
  return ((ucast.i>>23/*#bits mantissa*/) & ((1<<8/*#bits exponent*/)-1)) - 127;
}

static NOINLINE float
absf(const float flt)
{
  union { float f; int32_t i; } ucast;
  ucast.f = flt;
  ucast.i &= 0x7FFFFFFF;
  return ucast.f;
}

static FINLINE float
signf(const float flt)
{
  return flt < 0.f ? -1.f : 1.f;
}

static FINLINE double
sign(const double dbl)
{
  return dbl < 0.0 ? -1.0 : 1.0;
}

static FINLINE char
eq_eps(const double a, const double b, const double eps)
{
  return fabs(a - b) <= eps;
}

static FINLINE char
eq_epsf(const float a, const float b, const float eps)
{
  return absf(a - b) <= eps;
}

static FINLINE double
sin2cos(const double d)
{
  return sqrt(MMAX(0.0, 1.0 - d*d));
}

static FINLINE double
cos2sin(const double d)
{
  return sin2cos(d);
}

#endif /* MATH_H */
