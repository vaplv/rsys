/* Copyright (C) 2013-2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSys library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSys library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSys library. If not, see <http://www.gnu.org/licenses/>. */

#ifndef REALX_BEGIN_H
  #error The realX_begin.h header must be included
#endif

#undef REAL_EPSILON__
#undef REAL_EPSILON_double
#undef REAL_EPSILON_float
#undef REAL_EQ_EPS__
#undef REAL_EQ_EPS_double
#undef REAL_EQ_EPS_float
#undef REAL_LETTER__
#undef REAL_LETTER_double
#undef REAL_LETTER_float
#undef REAL_LETTER_TYPE_COMPATIBLE__
#undef REAL_TYPE__
#undef REAL_TYPE_COMPATIBLE__
#undef REAL_TYPE_COMPATIBLE_float
#undef REAL_TYPE_COMPATIBLE_double
#undef REALX_CAST__
#undef REALX_CTOR__
#undef REALX_DIMENSION__
#undef REALX_FUNC__
#undef REALX_REAL_FUNC__
#undef SIZEOF_REALX__

#undef REALX_BEGIN_H

